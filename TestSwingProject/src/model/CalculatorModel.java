package model;

public class CalculatorModel {
	private int variableA;
	private int variableB;

	public int getVariableA() {
		return variableA;
	}

	public void setVariableA(int variableA) {
		this.variableA = variableA;
	}

	public int getVariableB() {
		return variableB;
	}

	public void setVariableB(int variableB) {
		this.variableB = variableB;
	}

}
