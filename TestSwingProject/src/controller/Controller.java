package controller;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import model.CalculatorModel;
import view.CalculatorView;

public class Controller {
	private static CalculatorView view;
	private static CalculatorModel model;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view = new CalculatorView();
					view.setVisible(true);
					model = new CalculatorModel();
				} catch (Exception e) {
					e.printStackTrace();
				}

				view.getVariableA().addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						updateModel();
					}
				});

				view.getVariableB().addFocusListener(new FocusAdapter() {
					@Override
					public void focusLost(FocusEvent arg0) {
						updateModel();
					}
				});

				view.getBtnSum().addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						view.getResult().setText(String.valueOf(model.getVariableA() + model.getVariableB()));
					}
				});
			}
		});
	}

	public static void updateModel() {
		model.setVariableA(Integer.valueOf(view.getVariableA().getText()));
		model.setVariableB(Integer.valueOf(view.getVariableB().getText()));
	}
}
