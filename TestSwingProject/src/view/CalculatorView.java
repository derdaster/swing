package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Color;

public class CalculatorView extends JFrame {

	private JPanel contentPane;
	private JTextField variableA;
	private JTextField variableB;
	private JTextField result;
	private JButton btnSum;



	/**
	 * Create the frame.
	 */
	public CalculatorView() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBackground(Color.ORANGE);
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		variableA = new JTextField();
		
		variableA.setToolTipText("variable A");
		variableA.setBounds(130, 36, 86, 20);
		contentPane.add(variableA);
		variableA.setColumns(10);
		
		JLabel lblVariableA = new JLabel("Variable A");
		lblVariableA.setBounds(55, 39, 64, 14);
		contentPane.add(lblVariableA);
		
		variableB = new JTextField();
		variableB.setBounds(130, 67, 86, 20);
		contentPane.add(variableB);
		variableB.setColumns(10);
		
		JLabel lblVariableB = new JLabel("Variable B");
		lblVariableB.setBounds(55, 70, 64, 14);
		contentPane.add(lblVariableB);
		
		result = new JTextField();
		result.setEditable(false);
		result.setBounds(130, 98, 86, 20);
		contentPane.add(result);
		result.setColumns(10);
		
		JLabel lblResult = new JLabel("Result");
		lblResult.setBounds(55, 101, 64, 14);
		contentPane.add(lblResult);
		
		btnSum = new JButton("Sum");
		btnSum.setBounds(240, 97, 89, 23);
		contentPane.add(btnSum);
	}
	public JTextField getVariableA() {
		return variableA;
	}
	public JTextField getVariableB() {
		return variableB;
	}
	public JButton getBtnSum() {
		return btnSum;
	}
	public JTextField getResult() {
		return result;
	}
}
